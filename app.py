from flask import Flask, render_template, request, make_response, redirect
import security, config
from user import User

app = Flask(__name__)

@app.errorhandler(404)
def page_not_found(e):
	return render_template('error.html', error=404), 404

@app.errorhandler(500)
def internal_server_error(e):
	return render_template('error.html', error=500), 500

@app.route('/')
def home():

	pwd = 'secret'
	user = { id: 123, name: "Jane Doe" }

	pwd_hash = security.generate_pwd_hash()
	token = security.generate_token()

	print(pwd_hash)
	print(token)
	print(security.check_pwd_hash(pwd))
	print(security.check_token(token))


	#security.check_token(request.cookies.get('token'))
	# fwd user to appropriate page based on state
	return render_template('home.html')

@app.route('/account', methods=['GET', 'POST'])		
def account():
	# check token
	# show form if get param is edit
	return render_template('account.html')

@app.route('/create-account', methods=['GET', 'POST'])		
def create_account():
	u = User()
	pwd_hash = security.generate_pwd_hash('secret')
	return render_template('create_account.html')

@app.route('/deactivate-account', methods=['GET', 'POST'])		
def deactivate_account():
	# check token
	return render_template('deactivate_account.html')

@app.route('/forgot-password')		
def forgot_password():
	return render_template('forgot_password.html')

@app.route('/log-in', methods=['GET', 'POST'])		
def log_in():
	res = make_response(render_template('log_in.html'))
	# check token
	# find user
	u = User()
	u.get('tim@timsnyder.me')
	token = security.authorize(u)
	if token:
		res.set_cookie('secret-token', token.encode('utf-8'))
	else:
		print('no user or bad pwd')
	return res

@app.route('/log-out')		
def log_out():
	security.destroy_token()
	return render_template('log_out.html')

@app.route('/reset-password', methods=['GET', 'POST'])
def reset_password():
	return render_template('reset_password.html')

@app.route('/verify-email')		
def verify_email():
	return render_template('verify_email.html')

if __name__ == '__main__':
	app.run(debug=True, port=5000)