import config
import psycopg2
from psycopg2 import OperationalError

class User():
    def __init__(self):
        #id, fname, lname, email, is_active, is_reg, date_reg, pwd
        try:
            self.conn = psycopg2.connect(
                database = config.db_name,
                user = config.user,
                password = config.pwd,
                host = config.host,
                port = config.port
            )
            self.conn.autocommit = True
        except OperationalError as e:
            print(f"The error {e} occured.")

    # find user
    def get(self, email):
        if self.conn:
            cursor = self.conn.cursor()
            try:
                query = f"SELECT * FROM users WHERE email = '{email}';"
                cursor.execute(query)
                result = cursor.fetchall()
                if(len(result)):                   
                    self.id, self.fname, self.lname, self.email, self.is_active, self.is_reg, self.date_reg, self.pwd = result[0]
            except Exception as e:
                print(f"ERROR: '{e}' ")

    # log in
    def login(self):
        pass

    # create
    def create_user(self):
        pass

    # delete
    def delete_user(self):
        pass

    # update name
    def update_name(self):
        pass

    # update email
    def update_email(self):
        pass

    # update pwd
    def update_pwd(self):
        pass
